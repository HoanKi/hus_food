### Login with Facebook:
1. Curl in first time to get $code.
    $urlAuthorize = 'https://www.facebook.com/' .
        $CFG->graphApiVersion . '/dialog/oauth?' .
        'client_id=' . $CFG->client_id .
        '&redirect_uri=' . $CFG->wwwroot . '/login/facebook/redirect.php' .
        '&scope=public_profile+email';
    
2. Curl in second time to get Accesstoken
    $urlAccessToken = 'https://graph.facebook.com/' . $CFG->graphApiVersion . '/oauth/access_token';
    
    $param = array(
        'client_id' => $CFG->client_id,
        'redirect_uri' => $CFG->wwwroot . '/login/facebook/redirect.php',
        'client_secret' => $CFG->client_secret,
        'code' => $code
    );

    
3.  Verify accesstoken

    $verified = 0;
    $payload= $this -> verifyAndDecodeToken($accesstoken, true);
    if ($CFG->issVerify == $payload->iss && $CFG->scopesVerify == $payload->scp && time() < $payload->exp ){
        $verified = 1;
    }

4. Curl in third time to get UserInfor
    $fields = implode(',', [
            'id',
            'name',
            'first_name',
            'last_name',
            'email',
            'hometown',
            'picture.type(large){url}',
            'gender',
            'locale',
            'link',
        ]);
    
     $urlUserDetails = 'https://graph.facebook.com/' . $CFG->graphApiVersion . '/me?fields=' . $fields . '&access_token=' . $accesstoken;
     
     $name = $result->name;
     $firstName = $result->first_name;
     $lastName = $result->last_name;
     $email = $result->email;     