<?php
/**
 * Created by PhpStorm.
 * User: Hoan Ki
 * Date: 11-Oct-16
 * Time: 23:25
 */
require_once "../config.php";
global $CFG;
session_start();
if (isset($_SESSION[$CFG->token_login])) {

    unset($_SESSION[$CFG->token_login]);

    $redirect = $CFG->wwwroot . 'view/index.php';

    header("Location: ".$redirect);
}