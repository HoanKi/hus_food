<?php
/**
 * Created by PhpStorm.
 * User: Hoan Ki
 * Date: 09-Oct-16
 * Time: 18:33
 */


require_once "config.php";
global $CFG;
if (isset($_COOKIE[$CFG->token_login])) {
    session_start();
    $_SESSION[$CFG->token_login] = array(
        'user' => json_decode($_COOKIE[$CFG->token_login])
    );
}

header('Location: view/index.php');
