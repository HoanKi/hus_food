<?php

require_once '../../config.php';
require_once '../user.php';
global $CFG;

$code = $_GET['code'];
$param = array(
    'client_id' => $CFG->client_id,
    'redirect_uri' => $CFG->wwwroot . '/login/facebook/redirect.php',
    'client_secret' => $CFG->client_secret,
    'code' => $code
);

$urlAccessToken = 'https://graph.facebook.com/' . $CFG->graphApiVersion . '/oauth/access_token';

// Khởi tạo CURL
$ch = curl_init($urlAccessToken);

// Thiết lập có return
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Thiết lập sử dụng POST
curl_setopt($ch, CURLOPT_POST, count($param));

// Thiết lập các dữ liệu gửi đi
curl_setopt($ch, CURLOPT_POSTFIELDS, $param);

$result = curl_exec($ch);

curl_close($ch);

if ($result === false) {
    echo 'Curl error: ' . curl_error($ch);
} else {
    $result = json_decode($result);
    $accesstoken = $result->access_token;

    $fields = implode(',', [
        'id',
        'name',
        'first_name',
        'last_name',
        'email',
        'hometown',
        'picture.type(large){url}',
        'gender',
        'locale',
        'link',
    ]);

    $urlUserDetails = 'https://graph.facebook.com/' . $CFG->graphApiVersion . '/me?fields=' . $fields . '&access_token=' . $accesstoken;

// Khởi tạo CURL
    $chUser = curl_init($urlUserDetails);

// Thiết lập có return
    curl_setopt($chUser, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($chUser);

    curl_close($chUser);

    if ($result === false) {
        echo 'Curl error: ' . curl_error($chUser);

    } else {
        $result = json_decode($result);
        $name = $result->name;
        $firstName = $result->first_name;
        $lastName = $result->last_name;
        $email = $result->email;
        $picture = $result->picture;
        $gender = $result->gender;
        $user = new User($email, $name);

        session_start();
        $_SESSION[$CFG->token_login] = array(
            'user' => $user
        );

        setcookie($CFG->token_login, json_encode($user),time() + 1296000, "/","", 0);

        // Lấy path redirect
        $redirect = $CFG->wwwroot . 'view/index.php?userName=' . $user->name;
        header('Location:' . $redirect);
    }
}