<?php
require_once "../user.php";
require_once '../../config.php';
global $CFG;

$urlAuthorize = 'https://www.facebook.com/' .
    $CFG->graphApiVersion . '/dialog/oauth?' .
    'client_id=' . $CFG->client_id .
    '&redirect_uri=' . $CFG->wwwroot . '/login/facebook/redirect.php' .
    '&scope=public_profile+email';

header('Location: ' . $urlAuthorize);
