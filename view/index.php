<!DOCTYPE html>
<html>
<head>
    <title>Lozi</title>
    <meta charset="UTF-8">
    <style>

        div.container {
            width: 100%;
            border: 1px solid grey;
        }

        header, footer {
            padding: 1em;
            color: white;
            background-color: YellowGreen;
            clear: left;
            margin: 0;
        }

        header {
            text-align: right;
        }

        footer {
            text-align: center;
        }

        nav {
            float: left;
            padding: 10px;
            max-width: 250px;
            border-right: 1px solid gray;
        }

        nav ul {
            list-style-type: square;
            padding: 2em;
        }

        nav ul a:link {
            text-decoration: none;
        }

        nav ul a:hover {
            text-decoration: none;
            background-color: transparent;
        }

        article {
            margin-left: 170px;
            padding: 1em;
            overflow: hidden;
        }

        li a.menu:hover {
            text-decoration: none;
            background-color: GoldenRod;
        }

        li a.menu1:hover {
            text-decoration: underline;
        }

        li a.menu {
            display: block;
            color: white;
            text-align: center;
            padding: 16px;
            text-decoration: none;
        }

        li a.menu1 {
            color: white;
            text-align: center;
            padding: 16px;
            text-decoration: none;
        }

        li a {
            text-decoration: none;
        }

        li.id1 {
            float: left;
        }

        li.id0 {
            float: right;
        }

        ul.ul1 {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: Khaki;
            display: block;
            margin-left: 165px;
        }

        form {
            margin-right: 150px;
        }

        ul.ul0 {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: YellowGreen;
            margin-right: 150px;
            margin-left: 150px;
        }

        div.img {
            margin: 5px;
            border: 1px solid #ccc;
            float: left;
            width: 180px;
        }

        div.img:hover {
            border: 1px solid #777;
        }

        div.img img {
            width: 100%;
            height: 150px;
        }

        div.desc {
            padding: 15px;
            text-align: center;
        }

        input[type=text] {
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            font-size: 16px;
            background-color: white;

            background-position: 10px 10px;
            background-repeat: no-repeat;
            padding: 12px 20px 12px 40px;
            background-image: url('http://icons.iconarchive.com/icons/custom-icon-design/mini/32/Search-icon.png');
        }


    </style>

</head>
<body>
<div class="container">
    <header>

        <ul class="ul0">
            <?php
            require_once "../login/user.php";
            require_once "../config.php";
            global $CFG;

            session_start();

            if (!empty($_SESSION[$CFG->token_login])) {
                ?>

                <li class="id0"><a class="menu1"><?php echo $_SESSION[$CFG->token_login]['user']->name; ?></a>
                </li>

                <a href='../logout/logout.php'>
                    Đăng xuất</a>


            <?php } else { ?>
                <li class="id0"><a href="https://www.google.com.vn/?gfe_rd=cr&ei=YvnrV9eLHefc8geXtL_wBQ&gws_rd=ssl"
                                   class="menu1">Đăng nhập</a>
                </li>
                <li class="id0"><a href="<?php global $CFG;
                    echo $CFG->wwwroot ?>login/facebook/login.php"
                                   class="menu1">Đăng nhập với Facebook</a>
                </li>
                <li class="id0"><a href="https://www.google.com.vn/?gfe_rd=cr&ei=YvnrV9eLHefc8geXtL_wBQ&gws_rd=ssl"
                                   class="menu1">Đăng ký </a>
                </li>
            <?php } ?>

            <form>
                <label>Tìm kiếm </label> <input type="text" name="search" placeholder="Search..">
            </form>

        </ul>


        <ul class="ul1">

            <li class="id1"><a href="https://www.google.com.vn/?gfe_rd=cr&ei=YvnrV9eLHefc8geXtL_wBQ&gws_rd=ssl"
                               class="menu">Trang chủ </a></li>
            <li class="id1"><a href="https://www.google.com.vn/?gfe_rd=cr&ei=YvnrV9eLHefc8geXtL_wBQ&gws_rd=ssl"
                               class="menu">Giới thiệu</a></li>
            <li class="id1"><a href="https://www.google.com.vn/?gfe_rd=cr&ei=YvnrV9eLHefc8geXtL_wBQ&gws_rd=ssl"
                               class="menu">Dịch vụ </a></li>
            <li class="id1"><a href="https://www.google.com.vn/?gfe_rd=cr&ei=YvnrV9eLHefc8geXtL_wBQ&gws_rd=ssl"
                               class="menu">Tuyển dụng</a></li>
            <li class="id1"><a href="https://www.google.com.vn/?gfe_rd=cr&ei=YvnrV9eLHefc8geXtL_wBQ&gws_rd=ssl"
                               class="menu">Tin tức</a></li>
            <li class="id1"><a href="https://www.google.com.vn/?gfe_rd=cr&ei=YvnrV9eLHefc8geXtL_wBQ&gws_rd=ssl"
                               class="menu">Liên hệ</a></li>
        </ul>

    </header>
    <nav>
        <h3>Danh mục tìm kiếm</h3>
        <ul>

            <li><a href="https://www.google.com.vn/?gfe_rd=cr&ei=YvnrV9eLHefc8geXtL_wBQ&gws_rd=ssl" target="_blank">Ăn
                    với
                    người yêu</a></li>
            <li><a href="https://www.google.com.vn/?gfe_rd=cr&ei=YvnrV9eLHefc8geXtL_wBQ&gws_rd=ssl" target="_blank">Góc
                    cafe </a></li>
            <li><a href="https://www.google.com.vn/?gfe_rd=cr&ei=YvnrV9eLHefc8geXtL_wBQ&gws_rd=ssl" target="_blank">Ăn
                    với
                    gia đình</a></li>
            <li><a href="https://www.google.com.vn/?gfe_rd=cr&ei=YvnrV9eLHefc8geXtL_wBQ&gws_rd=ssl" target="_blank">Ăn
                    với
                    nhóm</a></li>
            <li><a href="https://www.google.com.vn/?gfe_rd=cr&ei=YvnrV9eLHefc8geXtL_wBQ&gws_rd=ssl" target="_blank">Ăn
                    một
                    mình</a></li>
            <li><a href="https://www.google.com.vn/?gfe_rd=cr&ei=YvnrV9eLHefc8geXtL_wBQ&gws_rd=ssl" target="_blank">Uống
                    với
                    người yêu</a></li>
            <li><a href="" target="_blank">Góc cafe </a></li>
            <li><a href="" target="_blank">Uống với gia đình</a></li>
            <li><a href="" target="_blank">Uống với nhóm</a></li>
            <li><a href="" target="_blank">Uống một mình</a></li>
        </ul>
        <h3>Sản phẩm bán chạy</h3>
        <ul>
            <li><a href="" target="_blank">Ăn với người yêu</a></li>
            <li><a href="" target="_blank">Góc cafe </a></li>
            <li><a href="" target="_blank">Ăn với gia đình</a></li>
        </ul>

        <h3>Tin nổi bật</h3>
        <ul>
            <li><a href="" target="_blank">Ăn với người yêu</a></li>
            <li><a href="" target="_blank">Góc cafe </a></li>
            <li><a href="" target="_blank">Ăn với gia đình</a></li>
        </ul>
        <h3>Hỗ trợ trực tuyến</h3>
        <ul>
            <li><a href="" target="_blank">Ăn với người yêu</a></li>
            <li><a href="https://www.google.com.vn/?gfe_rd=cr&ei=YvnrV9eLHefc8geXtL_wBQ&gws_rd=ssl" target="_blank">Góc
                    cafe </a></li>
            <li><a href="https://www.google.com.vn/?gfe_rd=cr&ei=YvnrV9eLHefc8geXtL_wBQ&gws_rd=ssl" target="_blank">Ăn
                    với
                    gia đình</a></li>
        </ul>
    </nav>
    <article>
        <div><h3 id="love" style="text-align:center"> Ăn với gia đình</h3></div>

        <div class="img">
            <a target="_blank"
               href="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8FcDKVWNT5N3rZKDSTmpjCbVBWETHnc-GIDnJjbKLJEeXXcAdVQ">
                <img
                    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8FcDKVWNT5N3rZKDSTmpjCbVBWETHnc-GIDnJjbKLJEeXXcAdVQ"
                    alt="Món ăn của người việt">
            </a>
            <div class="desc">Món ăn của người việt</div>
        </div>

        <div class="img">
            <a target="_blank"
               href="http://newsen.vn/data/news/2014/6/28/20/10-mon-an-phai-nem-khi-den-pho-am-thuc-Chinatown--Singapore-1-1403945877.jpg">
                <img
                    src="http://newsen.vn/data/news/2014/6/28/20/10-mon-an-phai-nem-khi-den-pho-am-thuc-Chinatown--Singapore-1-1403945877.jpg"
                    alt="Vịt quay Tiong Bahru">
            </a>
            <div class="desc">Vịt quay Tiong Bahru</div>

        </div>
        <div class="img">
            <a target="_blank"
               href="http://newsen.vn/data/news/2014/6/28/20/10-mon-an-phai-nem-khi-den-pho-am-thuc-Chinatown--Singapore-2-1403945877.jpg">
                <img
                    src="http://newsen.vn/data/news/2014/6/28/20/10-mon-an-phai-nem-khi-den-pho-am-thuc-Chinatown--Singapore-2-1403945877.jpg"
                    alt="Hàu chiên Katong">
            </a>
            <div class="desc">Hàu chiên Katong</div>

        </div>
        <div class="img">
            <a target="_blank"
               href="http://newsen.vn/data/news/2014/6/28/20/10-mon-an-phai-nem-khi-den-pho-am-thuc-Chinatown--Singapore-3-1403945878.jpg">
                <img
                    src="http://newsen.vn/data/news/2014/6/28/20/10-mon-an-phai-nem-khi-den-pho-am-thuc-Chinatown--Singapore-3-1403945878.jpg"
                    alt="Cháo ếch Geylang">
            </a>
            <div class="desc">Cháo ếch Geylang</div>

        </div>
        <div class="img">
            <a target="_blank"
               href="http://newsen.vn/data/news/2014/6/28/20/10-mon-an-phai-nem-khi-den-pho-am-thuc-Chinatown--Singapore-4-1403945878.jpg">
                <img
                    src="http://newsen.vn/data/news/2014/6/28/20/10-mon-an-phai-nem-khi-den-pho-am-thuc-Chinatown--Singapore-4-1403945878.jpg"
                    alt="Mì bò Odeon">
            </a>
            <div class="desc">Mì bò Odeon</div>

        </div>
        <div class="img">
            <a target="_blank"
               href="http://eva-img.24hstatic.com/upload/3-2016/images/2016-09-28/1-1475054596-width500height333.jpg">
                <img
                    src="http://eva-img.24hstatic.com/upload/3-2016/images/2016-09-28/1-1475054596-width500height333.jpg"
                    alt="Mì bò Odeon">
            </a>
            <div class="desc">Gỏi miến chân gà</div>

        </div>
        <div class="img">
            <a target="_blank"
               href="http://kingbbq.com.vn/wp-content/uploads/2014/08/mg_9252-a.jpg">
                <img src="http://kingbbq.com.vn/wp-content/uploads/2014/08/mg_9252-a.jpg"
                     alt="Bimbimbap – Cơm trộn">
            </a>
            <div class="desc">Bimbimbap</div>

        </div>
        <div class="img">
            <a target="_blank"
               href="http://kingbbq.com.vn/wp-content/uploads/2014/09/gimbab2-1024x751.jpg">
                <img src="http://kingbbq.com.vn/wp-content/uploads/2014/09/gimbab2-1024x751.jpg"
                     alt=" Gimbap – Cơm cuốn lá rong biển">
            </a>
            <div class="desc"> Gimbap</div>

        </div>
        <div class="img">
            <a target="_blank"
               href="http://eva-img.24hstatic.com/upload/3-2016/images/2016-09-28/2-1475054596-width500height344.jpg">
                <img
                    src="http://eva-img.24hstatic.com/upload/3-2016/images/2016-09-28/2-1475054596-width500height344.jpg"
                    alt="Canh bí đao tôm khô">
            </a>
            <div class="desc">Canh bí đao tôm khô</div>

        </div>
    </article>
    <footer>
        <p><strong>Nhóm 3 </strong><br><strong>334- Nguyễn Trãi- Thanh Xuân-Hà Nội</strong><br>
            Điện thoại:<strong> 0900 0101 </strong> Email hus.edu.vn</p>
    </footer>
</div>
</body>

</html>
